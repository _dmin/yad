#!/usr/bin/env bash

# zsh <(curl -s https://gitlab.com/_dmin/yad/raw/master/disk.sh)

# Pre-installation

device="/dev/sda"

sgdisk -Zo $device
sgdisk -n 1:0:+256M -c 1:"EFI System" -t 1:ef00 $device
sgdisk -n 2:0:+10G -c 2:"System" -t 2:8304 $device
sgdisk -n 3:0:0 -c 3:"Home" -t 3:8302 $device

mkfs.fat -F32 $device"1"
mkfs.ext4 $device"2"
mkfs.ext4 $device"3"

mount $device"2" /mnt

mkdir -p /mnt/{boot,home}

mount $device"1" /mnt/boot
mount $device"3" /mnt/home

lsblk -o +PTTYPE,FSTYPE,PARTLABEL $device

# Installation

pacstrap /mnt base

genfstab -U /mnt >> /mnt/etc/fstab

(cd /mnt && curl -O https://gitlab.com/_dmin/yad/raw/master/chroot.sh)

chmod +x /mnt/chroot.sh
arch-chroot /mnt ./chroot.sh $1