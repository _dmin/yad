#!/usr/bin/env bash

filename="chroot"
hostname="arch"

ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime

hwclock --systohc

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen

echo "LANG=en_US.UTF-8" >> /etc/locale.conf

echo "${hostname}" > /etc/hostname
echo -e "127.0.0.1 localhost\n::1 localhost\n127.0.1.1 ${hostname}.localdomain ${hostname}" >> /etc/hosts

passwd $1

# bootctl --path=/boot install
# bootctl update

rm -f $filename